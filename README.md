## Liatrio Demo Project
### The Task
Design an application hosted on k8's in the cloud that exposes an RESTFUL api. Eveything should be automated.
### The Solution
![](images/infra.png)
1. Infrasturcture
   1. EKS
      1. JS Application with exposed api accecable from the liatro loadbalancer: 
         1. :5000/api/msg -> {{ “message”: “Automate all the things!”“timestamp”: 1529729125 }}
   2. AWS ECR
      1. Container with JS Application
      2. You will not have access to Push to the repo that currently is being used, **in order to run this on your own you will need to create an ecr repo and update the following files:**
         1. .gitlab-ci.yml
            variables:
                **DOCKER_REGISTRY: public.ecr.aws/xxxxx**
                  AWS_DEFAULT_REGION: us-east-1
                **APP_NAME: liatrio**
         2. /terraform/eks/kubernetes.tf
        spec {
                container {
                **image = "public.ecr.aws/xxxxxx/liatrio:latest"**
                name  = "liatrio-container"
                port {
                    container_port = 5000
                }
            }
        }
   
2. Infrastructure Provisioning
   Infrastructure provision is done using Terraform, If you would like to create the infrastructure simply run the following commands from your terminal: 
        git clone git@gitlab.com:natejswenson/liatrio-demo.git
        cd liatrio-demo
        terraform init 
        terraform plan
        terraform apply
   1. eks-cluster.tf
      1. sets up the kubernetes cluster and 2 worker nodes 
   2. outputs.tf
   3. securitygroups.tf
      1. defines security groups for the worker groups
   4. versions.tf
      1. defines what versions we will be using for our providers
   5. vpc.tf
      1. definition of our vpc which will be created
   6. kubernettes.tf
      1. The application which will be deployed to the our cluster
   when you are through and wish to destroy the infra simply run the following command from the same terminal:
        terraform destroy'
    A few considerations if you do plan on running locally:
    1. you will need Terraform 1.2.9 installed
    2. you will need aws cli installed
3. CICD
   Gitlab ci is being used to build our dockerfile and push it to our ecr registry when it has been changed as well as deploying the latest version to our kubernetes cluster
### Using in your own repo
This code has been written in a way which makes it very versitile and easy to use in any repo.
1. Clone into your namespace on gitlab'
2. Add **AWS_ACCESS_KEY_ID** and **AWS_SECRET_ACCESS_KEY** to your CICD variables
3. Make sure you update .gitlab-ci.yml and kubernettes.tf to reflect the corect ecr registry

